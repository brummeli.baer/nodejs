# Nodejs application for BKW Openshift Demo

Minimale NodeJS Applikation.

Test Bash Script:
```{r, engine='bash', count_lines}
while : 
do 
   sleep 1
   curl http://welcome-welcome.apps.bkw.ch
done
```

URL:

* /healthz
* /cancel
